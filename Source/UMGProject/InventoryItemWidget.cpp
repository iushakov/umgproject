// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "InventoryItemWidget.h"

#include "Blueprint/WidgetTree.h"
#include "Components/CanvasPanel.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/Image.h"

#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"

FReply UInventoryItemWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	UE_LOG(LogTemp, Log, TEXT("UInventoryItemWidget:: OnMouseButtonDown"));
	if (IsFree)
	{
		return Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
	}
	else
	{
		return UWidgetBlueprintLibrary::DetectDragIfPressed(InMouseEvent, this, EKeys::LeftMouseButton).NativeReply;
	}	
}

void UInventoryItemWidget::NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation *& OutOperation)
{
	UE_LOG(LogTemp, Log, TEXT("UInventoryItemWidget:: OnDragDetected"));

	UDragDropOperation* Operation = NewObject<UDragDropOperation>();;
	if (Operation)
	{
		Operation->SetFlags(RF_StrongRefOnFrame);
		Operation->Payload = this;
		Operation->DefaultDragVisual = WidgetTree->ConstructWidget<UImage>();
		OutOperation = Operation;
	}
}

TSharedRef<SWidget> UInventoryItemWidget::RebuildWidget()
{
	if (WidgetTree)
	{
		WidgetTree->ForEachWidget([&](UWidget* Widget) {
			UCanvasPanel* CanvasPanel = Cast<UCanvasPanel>(Widget);
			if (CanvasPanel)
			{
				UImage* Image = WidgetTree->ConstructWidget<UImage>();

				UCanvasPanelSlot* PanelSlot = CanvasPanel->AddChildToCanvas(Image);
				PanelSlot->SetAnchors(FAnchors(0.f, 0.f, 1.f, 1.f));
				PanelSlot->SetOffsets(FMargin(0.f, 0.f));
			}
		});
	}

	return Super::RebuildWidget();
}
