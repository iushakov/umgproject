// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "BoxActor.h"

#include "PickUpComponent.h"

// Sets default values
ABoxActor::ABoxActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	PickUpComponent = CreateDefaultSubobject<UPickUpComponent>(TEXT("PickComponent"));
	AddOwnedComponent(PickUpComponent);
}
