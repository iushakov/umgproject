// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "HealthBarWidget.h"

#include "SHealthBarWidget.h"

void UHealthBarWidget::SetPercent(float InPercent)
{
	Percent = InPercent;
	if (MyHealthBarWidget.IsValid())
	{
		MyHealthBarWidget->SetPercent(InPercent);
	}
}

void UHealthBarWidget::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	TAttribute< TOptional<float> > PercentBinding = OPTIONAL_BINDING_CONVERT(float, Percent, TOptional<float>, ConvertFloatToOptionalFloat);
	MyHealthBarWidget->SetPercent(PercentBinding);
}

void UHealthBarWidget::ReleaseSlateResources(bool bReleaseChildren)
{
	Super::ReleaseSlateResources(bReleaseChildren);

	MyHealthBarWidget.Reset();
}

TSharedRef<SWidget> UHealthBarWidget::RebuildWidget()
{
	MyHealthBarWidget = SNew(SHealthBarWidget);
	return MyHealthBarWidget.ToSharedRef();
}
