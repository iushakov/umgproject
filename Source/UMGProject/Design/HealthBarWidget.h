// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/Widget.h"
#include "HealthBarWidget.generated.h"

/**
 * 
 */
UCLASS()
class UMGPROJECT_API UHealthBarWidget : public UWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Progress, meta = (UIMin = "0", UIMax = "1"))
	float Percent;

public:
	UFUNCTION(BlueprintCallable, Category = "Progress")
	void SetPercent(float InPercent);

	UPROPERTY()
	FGetFloat PercentDelegate;

public:
	virtual void SynchronizeProperties() override;

	virtual void ReleaseSlateResources(bool bReleaseChildren) override;
	
protected:
	virtual TSharedRef<SWidget> RebuildWidget() override;

protected:
	TSharedPtr<class SHealthBarWidget> MyHealthBarWidget;
};
