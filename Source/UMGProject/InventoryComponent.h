// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

DECLARE_DELEGATE_OneParam( FComponentOnAddSignature, int32 );
DECLARE_DELEGATE_OneParam( FComponentOnRemoveSignature, int32 );

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UMGPROJECT_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	FComponentOnAddSignature OnAdd;
	FComponentOnRemoveSignature OnRemove;

	// Sets default values for this component's properties
	UInventoryComponent();

	int32 GetNumberOfItems() const;

	class FInventoryItem* GetFreeItem();

	class FInventoryItem* GetItem(int32 ItemId);

	void RemoveItem(int32 ItemId);

	TArray<int32> GetState() const;
	void SetState(const TArray<int32>& State);

private:
	TArray<class FInventoryItem*> Items;
};
