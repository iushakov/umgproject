// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BTTask_Trade.h"

#include "TraderCharacter.h"
#include "TraderAIController.h"
#include "UMGProjectCharacter.h"
#include "UMGProjectPlayerController.h"
#include "InventoryComponent.h"
#include "InventoryItem.h"

EBTNodeResult::Type UBTTask_Trade::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ATraderAIController* TraderAIController = Cast<ATraderAIController>(OwnerComp.GetAIOwner());
	if (TraderAIController == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	ATraderCharacter* TraderCharacter = Cast<ATraderCharacter>(TraderAIController->GetCharacter());
	AUMGProjectCharacter* PlayerCharacter = Cast<AUMGProjectCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(TraderAIController->TraderKeyID));
	if (TraderCharacter == nullptr || PlayerCharacter == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	UInventoryComponent* TraderInventory = TraderCharacter->FindComponentByClass<UInventoryComponent>();

	AUMGProjectPlayerController* PlayerController = Cast<AUMGProjectPlayerController>(PlayerCharacter->GetController());
	UInventoryComponent* PlayerInventory = PlayerController->FindComponentByClass<UInventoryComponent>();

	if (TraderInventory == nullptr || PlayerInventory == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	FInventoryItem* TradeItem = TraderInventory->GetItem(0);
	if (TradeItem != nullptr)
	{
		FInventoryItem* FreeItem = PlayerInventory->GetFreeItem();
		if (FreeItem)
		{
			FreeItem->Take(TradeItem->GetName(), TradeItem->GetClass());
			TraderInventory->RemoveItem(TradeItem->GetId());

			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}
