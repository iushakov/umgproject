// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/BTService.h"
#include "CheckForPlayerService.generated.h"

/**
 * 
 */
UCLASS()
class UMGPROJECT_API UCheckForPlayerService : public UBTService
{
	GENERATED_BODY()
	
public:
	UCheckForPlayerService();

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
