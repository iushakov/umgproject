// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"

#include "TraderCharacter.h"
#include "TraderAIController.h"

ATraderAIController::ATraderAIController()
{
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));
	BehaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTreeComponent"));
}

void ATraderAIController::Possess(APawn *InPawn)
{
	Super::Possess(InPawn);

	ATraderCharacter* TraderCharacter = Cast<ATraderCharacter>(InPawn);
	if (TraderCharacter != nullptr && TraderCharacter->Behavior != nullptr)
	{
		BlackboardComponent->InitializeBlackboard(*TraderCharacter->Behavior->BlackboardAsset);
		TraderKeyID = BlackboardComponent->GetKeyID("Target");
		BehaviorTreeComponent->StartTree(*TraderCharacter->Behavior);
	}
}
