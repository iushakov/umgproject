// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class UMGPROJECT_API FInventoryItem
{
public:
	FInventoryItem(int32 Id);

	int32 GetId() const;

	FString GetName() const;

	UClass* GetClass() const;

	bool GetIsFree() const;

	void Take(const FString& Name, UClass* Class);

	void Return();

private:
	int32 Id;
	bool IsFree;
	FString Name;
	UClass* Class;
};
