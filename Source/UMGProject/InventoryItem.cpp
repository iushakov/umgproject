// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "InventoryItem.h"

FInventoryItem::FInventoryItem(int32 Id) : Id(Id), IsFree(true), Name(FString()), Class(nullptr) {}

int32 FInventoryItem::GetId() const
{
	return Id;
}

bool FInventoryItem::GetIsFree() const
{
	return IsFree;
}

FString FInventoryItem::GetName() const
{
	return Name;
}

UClass* FInventoryItem::GetClass() const
{
	return Class;
}

void FInventoryItem::Take(const FString& Name, UClass* Class)
{
	this->IsFree = false;
	this->Name = Name;
	this->Class = Class;
}

void FInventoryItem::Return()
{
	this->IsFree = true;
}
