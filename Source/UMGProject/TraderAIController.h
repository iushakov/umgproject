// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "TraderAIController.generated.h"

/**
 * 
 */
UCLASS()
class UMGPROJECT_API ATraderAIController : public AAIController
{
	GENERATED_BODY()

	UPROPERTY(transient)
	class UBlackboardComponent* BlackboardComponent;
	
	UPROPERTY(transient)
	class UBehaviorTreeComponent* BehaviorTreeComponent;
	
public:
	ATraderAIController();

	virtual void Possess(APawn *InPawn) override;

	uint8 TraderKeyID;

};
