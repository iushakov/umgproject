// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "InventoryWidget.h"

#include "Blueprint/WidgetTree.h"
#include "Blueprint/DragDropOperation.h"

#include "Components/GridPanel.h"
#include "Components/GridSlot.h"
#include "Components/SizeBox.h"

#include "InventoryItemWidget.h"

bool UInventoryWidget::NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	UE_LOG(LogTemp, Log, TEXT("UInventoryWidget:: OnDrop"));

	UInventoryItemWidget* ItemWidget = Cast<UInventoryItemWidget>(InOperation->Payload);
	if (ItemWidget)
	{
		OnDrop.ExecuteIfBound(ItemWidget->ItemId);
	}

	return true;
}

TSharedRef<SWidget> UInventoryWidget::RebuildWidget()
{
	if (WidgetTree)
	{
		WidgetTree->ForEachWidget([&](UWidget* Widget) {
			UGridPanel* GridPanel = Cast<UGridPanel>(Widget);
			if (GridPanel)
			{
				UE_LOG(LogTemp, Log, TEXT("UInventoryWidget:: GridPanel Setup"));

				GridPanel->RowFill.Add(1.f);

				for (int32 Column = 0; Column < 8; Column++)
				{
					GridPanel->ColumnFill.Add(1.f);
				}

				for (int32 Column = 0; Column < 8; Column++)
				{
					USizeBox* SizeBox = WidgetTree->ConstructWidget<USizeBox>();
					SizeBox->bOverride_WidthOverride = 1;
					SizeBox->WidthOverride = 100.f;
					SizeBox->bOverride_HeightOverride = 1;
					SizeBox->HeightOverride = 100.f;

					UInventoryItemWidget* ItemWidget = WidgetTree->ConstructWidget<UInventoryItemWidget>(ItemClass);
					ItemWidget->ItemId = -1;
					ItemWidget->IsFree = true;
					Items.Add(ItemWidget);

					SizeBox->AddChild(ItemWidget);

					UGridSlot* GridSlot = GridPanel->AddChildToGrid(SizeBox);
					GridSlot->Padding = FMargin(5.f, 5.f, 5.f, 5.f);
					GridSlot->Row = 0;
					GridSlot->Column = Column;

					GridSlot->HorizontalAlignment = EHorizontalAlignment::HAlign_Fill;
					GridSlot->VerticalAlignment = EVerticalAlignment::VAlign_Fill;
				}

				OnReady.ExecuteIfBound();
			}
		});
	}

	return Super::RebuildWidget();
}

void UInventoryWidget::OnAdd(int32 ItemId)
{
	UE_LOG(LogTemp, Log, TEXT("UInventoryWidget:: OnAdd %d"), ItemId);

	for (auto ItemWidget : Items)
	{
		if (ItemWidget->IsFree)
		{
			ItemWidget->SetColorAndOpacity(FLinearColor(0.f, 0.f, 1.f));
			ItemWidget->ItemId = ItemId;
			ItemWidget->IsFree = false;
			break;
		}
	}
}

void UInventoryWidget::OnRemove(int32 ItemId)
{
	UE_LOG(LogTemp, Log, TEXT("UInventoryWidget:: OnRemove %d"), ItemId);

	UInventoryItemWidget** ItemWidget = Items.FindByPredicate([ItemId](const UInventoryItemWidget* Item) { return Item->ItemId == ItemId; });
	if (ItemWidget)
	{
		(*ItemWidget)->SetColorAndOpacity(FLinearColor(1.f, 1.f, 1.f));
		(*ItemWidget)->ItemId = -1;
		(*ItemWidget)->IsFree = true;
	}
}
