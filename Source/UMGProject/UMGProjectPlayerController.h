// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/PlayerController.h"
#include "UMGProjectPlayerController.generated.h"

UCLASS()
class AUMGProjectPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AUMGProjectPlayerController();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<class UInventoryWidget> InventoryWidgetClass;

	virtual void BeginPlay() override;

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	void OnInventory();
	void OnSaveGame();

	void OnInventoryReady();
	void OnInventoryDrop(int32 ItemId);

private:
	class UInventoryComponent* Inventory;
	class UInventoryWidget* InventoryWidget;
};


