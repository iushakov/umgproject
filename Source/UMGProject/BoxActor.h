// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BoxActor.generated.h"

UCLASS()
class UMGPROJECT_API ABoxActor : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere)
	class UPickUpComponent* PickUpComponent;

	// Sets default values for this actor's properties
	ABoxActor();

};
