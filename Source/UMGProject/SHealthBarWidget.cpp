// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "SHealthBarWidget.h"
#include "SlateOptMacros.h"
#include "Rendering/DrawElements.h"

#include "SlateBasics.h"
#include "SlateExtras.h"

void SHealthBarWidget::Construct(const FArguments& InArgs)
{
	ChildSlot
		.VAlign(VAlign_Fill)
		.HAlign(HAlign_Fill);
}

void SHealthBarWidget::SetPercent(TAttribute< TOptional<float> > InPercent)
{
	if (!Percent.IdenticalTo(InPercent))
	{
		Percent = InPercent;
		Invalidate(EInvalidateWidget::LayoutAndVolatility);
	}
}

void PushTransformedClip(FSlateWindowElementList& OutDrawElements, const FGeometry& AllottedGeometry, FVector2D InsetPadding, FVector2D ProgressOrigin, FSlateRect Progress)
{
	const FSlateRenderTransform& Transform = AllottedGeometry.GetAccumulatedRenderTransform();

	const FVector2D MaxSize = AllottedGeometry.GetLocalSize() - (InsetPadding * 2.0f);

	OutDrawElements.PushClip(FSlateClippingZone(
		Transform.TransformPoint(InsetPadding + (ProgressOrigin - FVector2D(Progress.Left, Progress.Top)) * MaxSize),
		Transform.TransformPoint(InsetPadding + FVector2D(ProgressOrigin.X + Progress.Right, ProgressOrigin.Y - Progress.Top) * MaxSize),
		Transform.TransformPoint(InsetPadding + FVector2D(ProgressOrigin.X - Progress.Left, ProgressOrigin.Y + Progress.Bottom) * MaxSize),
		Transform.TransformPoint(InsetPadding + (ProgressOrigin + FVector2D(Progress.Right, Progress.Bottom)) * MaxSize)
	));
}

int32 SHealthBarWidget::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	SCompoundWidget::OnPaint(Args, AllottedGeometry, MyClippingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);

	FSlateColorBrush Brush(FLinearColor(1.f, 0.f, 0.f));
	FSlateRect Frame = MyClippingRect;

	FSlateDrawElement::MakeBox(
		OutDrawElements,
		++LayerId,
		AllottedGeometry.ToPaintGeometry(),
		&Brush,
		Frame,
		ESlateDrawEffect::None,
		FLinearColor::Black
	);

	TOptional<float> ProgressFraction = Percent.Get();
	if (ProgressFraction.IsSet())
	{
		const float ClampedFraction = FMath::Clamp(ProgressFraction.GetValue(), 0.0f, 1.0f);

		PushTransformedClip(OutDrawElements, AllottedGeometry, FVector2D(0, 0), FVector2D(0, 0), FSlateRect(0, 0, ClampedFraction, 1));

		FSlateDrawElement::MakeBox(
			OutDrawElements,
			++LayerId,
			AllottedGeometry.ToPaintGeometry(
				FVector2D::ZeroVector, 
				FVector2D(AllottedGeometry.GetLocalSize().X, AllottedGeometry.GetLocalSize().Y)),
			&Brush,
			Frame,
			ESlateDrawEffect::None,
			FLinearColor::Red
		);

		OutDrawElements.PopClip();
	}

	return LayerId;
}
