// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "InventoryWidget.generated.h"

DECLARE_DELEGATE( FComponentOnReadySignature )
DECLARE_DELEGATE_OneParam( FComponentOnDropSignature, int32 );

/**
 * 
 */
UCLASS()
class UMGPROJECT_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	FComponentOnReadySignature OnReady;
	FComponentOnDropSignature OnDrop;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class UInventoryItemWidget> ItemClass;

	virtual bool NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;

public:
	void OnAdd(int32 ItemId);
	void OnRemove(int32 ItemId);

protected:
	virtual TSharedRef<SWidget> RebuildWidget() override;

private:
	TArray<class UInventoryItemWidget*> Items;
};
