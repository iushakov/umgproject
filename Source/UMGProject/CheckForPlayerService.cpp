// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "CheckForPlayerService.h"

#include "TraderCharacter.h"
#include "TraderAIController.h"
#include "UMGProjectCharacter.h"

UCheckForPlayerService::UCheckForPlayerService()
{
	bCreateNodeInstance = true;
}

void UCheckForPlayerService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	ATraderAIController* TraderAIController = Cast<ATraderAIController>(OwnerComp.GetAIOwner());
	if (TraderAIController != nullptr)
	{
		AUMGProjectCharacter* PlayerCharacter = Cast<AUMGProjectCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
		if (PlayerCharacter != nullptr)
		{
			FVector TraderLocation = TraderAIController->GetCharacter()->GetActorLocation();
			FVector PlayerLocation = PlayerCharacter->GetActorLocation();

			if (fabs(FVector::Dist2D(TraderLocation, PlayerLocation)) < 130)
			{
				OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(TraderAIController->TraderKeyID, PlayerCharacter);
			}			
		}
	}
}
