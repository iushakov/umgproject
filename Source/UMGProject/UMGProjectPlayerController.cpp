// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "UMGProject.h"
#include "UMGProjectPlayerController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "UMGProjectCharacter.h"

#include "InventoryComponent.h"
#include "InventoryItem.h"
#include "InventoryWidget.h"
#include "PickUpComponent.h"
#include "LocalSaveGame.h"

AUMGProjectPlayerController::AUMGProjectPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	Inventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	AddOwnedComponent(Inventory);
}

void AUMGProjectPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (InventoryWidgetClass)
	{
		InventoryWidget = CreateWidget<UInventoryWidget>(this, InventoryWidgetClass);
		if (!InventoryWidget)
		{
			return;
		}

		InventoryWidget->OnReady.BindUObject(this, &AUMGProjectPlayerController::OnInventoryReady);
		InventoryWidget->OnDrop.BindUObject(this, &AUMGProjectPlayerController::OnInventoryDrop);

		Inventory->OnAdd.BindUObject(InventoryWidget, &UInventoryWidget::OnAdd);
		Inventory->OnRemove.BindUObject(InventoryWidget, &UInventoryWidget::OnRemove);

		InventoryWidget->AddToViewport();
		InventoryWidget->SetVisibility(ESlateVisibility::Hidden);
	}
}

void AUMGProjectPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void AUMGProjectPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AUMGProjectPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AUMGProjectPlayerController::OnSetDestinationReleased);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AUMGProjectPlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AUMGProjectPlayerController::MoveToTouchLocation);

	InputComponent->BindKey(EKeys::E, EInputEvent::IE_Released, this, &AUMGProjectPlayerController::OnInventory);
	InputComponent->BindKey(EKeys::S, EInputEvent::IE_Released, this, &AUMGProjectPlayerController::OnSaveGame);
}

void AUMGProjectPlayerController::MoveToMouseCursor()
{
	// Trace to see what is under the mouse cursor
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	if (Hit.bBlockingHit)
	{
		if (Hit.Actor != nullptr)
		{
			UPickUpComponent* PickUpComponent = Hit.Actor->FindComponentByClass<UPickUpComponent>();
			if (PickUpComponent != nullptr)
			{
				FInventoryItem* Item = Inventory->GetFreeItem();
				if (Item)
				{
					Item->Take(TEXT(""), Hit.Actor->GetClass());
					Hit.Actor->Destroy();

					bMoveToMouseCursor = false;
					return;
				}
				else
				{
					// TODO show alert "Inventory is full"
				}
			}
		}

		// We hit something, move there
		SetNewMoveDestination(Hit.ImpactPoint);
	}
}

void AUMGProjectPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void AUMGProjectPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void AUMGProjectPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void AUMGProjectPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void AUMGProjectPlayerController::OnInventory()
{
	if (InventoryWidget->Visibility == ESlateVisibility::Hidden)
	{
		InventoryWidget->SetVisibility(ESlateVisibility::Visible);
	}
	else if (InventoryWidget->Visibility == ESlateVisibility::Visible)
	{
		InventoryWidget->SetVisibility(ESlateVisibility::Hidden);
	}
}

void AUMGProjectPlayerController::OnSaveGame()
{
	ULocalSaveGame* SaveGame = Cast<ULocalSaveGame>(UGameplayStatics::CreateSaveGameObject(ULocalSaveGame::StaticClass()));
	SaveGame->InventoryState = Inventory->GetState();
	UE_LOG(LogTemp, Log, TEXT("AUMGProjectPlayerController:: Saving Game In %s"), *SaveGame->SaveSlotName);
	UGameplayStatics::SaveGameToSlot(SaveGame, SaveGame->SaveSlotName, SaveGame->UserIndex);
}

void AUMGProjectPlayerController::OnInventoryReady()
{
	ULocalSaveGame* SaveGame = Cast<ULocalSaveGame>(UGameplayStatics::CreateSaveGameObject(ULocalSaveGame::StaticClass()));
	UE_LOG(LogTemp, Log, TEXT("AUMGProjectPlayerController:: Loading Game From %s"), *SaveGame->SaveSlotName);
	SaveGame = Cast<ULocalSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveGame->SaveSlotName, SaveGame->UserIndex));
	if (SaveGame != nullptr && SaveGame->InventoryState.Num() != 0)
	{
		UE_LOG(LogTemp, Log, TEXT("AUMGProjectPlayerController:: Has Inventory State"));
		Inventory->SetState(SaveGame->InventoryState);
	}	
}

void AUMGProjectPlayerController::OnInventoryDrop(int32 ItemId)
{
	Inventory->RemoveItem(ItemId);
}
