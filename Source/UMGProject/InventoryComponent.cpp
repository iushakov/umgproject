// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGProject.h"
#include "InventoryComponent.h"

#include "InventoryItem.h"

UInventoryComponent::UInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	for (int32 Id = 0; Id < GetNumberOfItems(); Id++)
	{
		Items.Add(new FInventoryItem(Id));
	}
}

int32 UInventoryComponent::GetNumberOfItems() const
{
	return 8;
}

FInventoryItem* UInventoryComponent::GetFreeItem()
{
	FInventoryItem** Item = Items.FindByPredicate([](const FInventoryItem* Item) { return Item->GetIsFree(); });
	if (Item)
	{
		OnAdd.ExecuteIfBound((*Item)->GetId());
		return *Item;
	}
	
	return nullptr;
}

FInventoryItem* UInventoryComponent::GetItem(int32 ItemId)
{
	FInventoryItem** Item = Items.FindByPredicate([ItemId](const FInventoryItem* Item) { return !Item->GetIsFree() && Item->GetId() == ItemId; });
	if (Item)
	{
		return *Item;
	}

	return nullptr;
}

void UInventoryComponent::RemoveItem(int32 ItemId)
{
	FInventoryItem** Item = Items.FindByPredicate([ItemId](const FInventoryItem* Item) { return Item->GetId() == ItemId; });
	if (Item)
	{
		(*Item)->Return();
		OnRemove.ExecuteIfBound((*Item)->GetId());
	}
}

TArray<int32> UInventoryComponent::GetState() const
{
	TArray<int32> Result;
	for (int32 Index = 0; Index < Items.Num(); Index++)
	{
		Result.Emplace(Items[Index]->GetIsFree() ? -1 : Items[Index]->GetId());
	}
	return Result;
}

void UInventoryComponent::SetState(const TArray<int32>& State)
{
	for (int32 Index = 0; Index < Items.Num(); Index++)
	{
		if (State[Index] != -1)
		{
			Items[Index]->Take(TEXT(""), nullptr);
			OnAdd.ExecuteIfBound(Items[Index]->GetId());
		}		
	}
}
