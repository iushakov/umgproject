// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "UMGProject.h"
#include "UMGProjectGameMode.h"

#include "Blueprint/UserWidget.h"

AUMGProjectGameMode::AUMGProjectGameMode()
{

}

void AUMGProjectGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (HUDWidgetClass != nullptr)
	{
		HUDWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);
		if (HUDWidget != nullptr)
		{
			HUDWidget->AddToViewport();
		}
	}
}
